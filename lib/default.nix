{ pkgs, lib }:
let
  evalModules' = lib.evalModules;

  self = rec
  {
    # Module evaluation

    evalModules = modules:
      evalModules' {
        modules = [ (import ../modules) ] ++ modules;
        specialArgs.n2k = self;
        specialArgs.pkgs = pkgs;
      };

    generateOutputs = modules:
      let
        evaluated = evalModules modules;
        output = evaluated.config.kubernetes.output;

        pushImages =
          if evaluated.config.registry.willPushImages
          then evaluated.config.registry.pushImages
          else null;
      in
      output // { inherit pushImages; };

    generate = modules: (generateOutputs modules).manifest;

    # Options

    tyUnset = lib.types.mkOptionType {
      name = "unset";
      check = x: builtins.isAttrs x && x ? __k_unset__ && x.__k_unset__ == true;
    };
    unsetOr = inner: lib.types.either tyUnset inner;

    unset = { __k_unset__ = true; };

    filterObject = v:
      if builtins.isAttrs v
      then
        let
          filtered = lib.filterAttrs (_: v: v != unset) v;
        in
        lib.mapAttrs (_: filterObject) filtered
      else if builtins.isList v
      then builtins.map filterObject v
      else v;

    # NixOS

    nixosSystem = lib.nixosSystem;
  };
in
self
