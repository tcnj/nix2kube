{
  kubernetes.global.namespaces.tailscale = { };
  kubernetes.namespaces.kube-system.helmcharts.tailscale-operator.spec = {
    repo = "https://pkgs.tailscale.com/helmcharts";
    chart = "tailscale-operator";
    targetNamespace = "tailscale";
    valuesContent = builtins.toJSON {
      oauth = {
        clientId = "<fake>";
        clientSecret = "<fake>";
      };
    };
  };
}
