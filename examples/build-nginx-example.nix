{ config, pkgs, ... }:
{
  registry.defaultPrefix = "registry.gitlab.com/tcnj/nix2kube/build-nginx-example";
  kubernetes.output.globalLabels.deployment = "nginx-example";

  images.nginx.image =
    let
      nginxConfig = pkgs.writeText "nginx.conf" ''
        daemon off;
        events {}
        error_log /dev/stderr;
        pid /nginx.pid;
        http {
          access_log /dev/stdout;
          
          client_body_temp_path /tmp/;
          proxy_temp_path /tmp/;
          fastcgi_temp_path /tmp/;
          uwsgi_temp_path /tmp/;
          scgi_temp_path /tmp/;

          server {
            listen 80;

            location / {
              return 200 'Hello Kubernetes!';
            }
          }
        }
      '';
    in
    pkgs.dockerTools.buildLayeredImage {
      name = "nginx";
      config.Cmd = [ "${pkgs.nginx}/bin/nginx" "-c" nginxConfig ];
      fakeRootCommands = ''
        #!${pkgs.runtimeShell}
        ${pkgs.dockerTools.shadowSetup}
        groupadd -r nogroup
        useradd -r -g nogroup nobody
        mkdir /tmp
      '';
      enableFakechroot = true;
    };

  kubernetes.global.namespaces.nginx-example = { };

  kubernetes.namespaces.nginx-example = {
    deployments.nginx.spec = {
      replicas = 1;
      selector.matchLabels.app = "nginx";
      template = {
        metadata.labels.app = "nginx";
        spec = {
          containers = [{
            name = "nginx";
            image = config.images.nginx.registryPath;
            ports = [{ containerPort = 80; }];
          }];
        };
      };
    };

    services.nginx.spec = {
      selector.app = "nginx";
      ports = [{
        name = "http";
        protocol = "TCP";
        port = 80;
      }];
      type = "LoadBalancer";
      loadBalancerClass = "tailscale";
    };
  };
}
