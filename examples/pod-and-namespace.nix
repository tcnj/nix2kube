{
  kubernetes = {
    global.namespaces.my-ns = { };

    namespaces.my-ns.pods.my-pod = {
      spec.containers = [{
        name = "nginx";
        image = "nginx:1.14.2";
        ports = [{
          containerPort = 80;
        }];
      }];
    };
  };
}
