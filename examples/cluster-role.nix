{
  kubernetes.global.clusterroles.secret-reader = {
    rules = [{
      apiGroups = [ "" ];
      resources = [ "secrets" ];
      verbs = [ "get" "watch" "list" ];
    }];
  };
}
