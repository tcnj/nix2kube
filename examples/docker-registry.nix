{ lib, config, ... }:
with lib;
let
  cfg = config.apps.docker-registry;
in
{
  options.apps.docker-registry = {
    enabled = (mkEnableOption "docker registry") // { default = true; };

    namespace = mkOption {
      description = mdDoc "Namespace for the registry deployment.";
      type = types.str;
      default = "docker-registry";
    };
  };

  config = mkIf cfg.enabled {
    kubernetes.global.namespaces."${cfg.namespace}" = { };

    kubernetes.namespaces."${cfg.namespace}" = {
      persistentvolumeclaims.registry-data.spec = {
        accessModes = [ "ReadWriteOnce" ];
        storageClassName = "local-path";
        resources.requests.storage = "5Gi";
      };

      deployments.registry = {
        metadata.labels.app = "registry";

        spec = {
          replicas = 1;
          selector.matchLabels.app = "registry";
          template = {
            metadata.labels.app = "registry";
            spec = {
              containers = [{
                name = "registry";
                image = "registry:2";
                ports = [{ containerPort = 5000; }];
                volumeMounts = [{
                  name = "registry-data";
                  mountPath = "/var/lib/registry";
                  subPath = "registry";
                }];
              }];
              volumes = [{
                name = "registry-data";
                persistentVolumeClaim.claimName = "registry-data";
              }];
            };
          };
        };
      };

      services.registry.spec = {
        # type = "LoadBalancer";
        selector.app = "registry";
        ports = [{
          name = "registry-tcp";
          protocol = "TCP";
          port = 5000;
          targetPort = 5000;
        }];
        # loadBalancerClass = "tailscale";
      };

      ingresses.registry.spec = {
        defaultBackend.service = {
          name = "registry";
          port.number = 5000;
        };
        ingressClassName = "tailscale";
        tls = [{ hosts = [ "docker-registry" ]; }];
      };
    };
  };
}
