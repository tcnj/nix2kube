{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config;

  imageSubmodule = { name, config, ... }: {
    options = {
      image = mkOption {
        description = mdDoc "Image package, typically generated using dockerTools.buildLayeredImage";
        type = types.package;
      };

      name = mkOption {
        description = mdDoc "Container image name";
        type = types.str;
        default = builtins.unsafeDiscardStringContext config.image.imageName;
        defaultText = "image.name";
      };

      tag = mkOption {
        description = mdDoc "Container image tag";
        type = types.str;
        default = builtins.unsafeDiscardStringContext config.image.imageTag;
        defaultText = "image.tag";
      };

      registryPrefix = mkOption {
        description = mdDoc "Registry path prefix under which the image is published";
        type = types.str;
        default = cfg.registry.defaultPrefix;
        defaultText = literalExpression "registry.defaultPrefix";
      };

      registryPath = mkOption {
        description = mdDoc "Full path to the image in the container registry";
        type = types.str;
        default = "${config.registryPrefix}/${config.name}:${config.tag}";
        defaultText = literalExpression ''"''${config.registryPrefix}/''${config.name}:''${config.tag}"'';
      };

      enablePush = mkOption {
        description = mdDoc "Whether to include this image in the registry.pushImages script.";
        type = types.bool;
        default = true;
      };

      podmanScript = mkOption {
        description = mdDoc "A script for running this container image with podman. Designed for testing/debudding purposes.";
        type = types.package;
        default = pkgs.writeShellScript "${name}-run-in-podman.sh" ''
          ${pkgs.podman}/bin/podman run $@ docker-archive:${config.image}
        '';
      };
    };
  };

  allImages = builtins.attrValues config.images;
  enabledImages = builtins.filter (image: image.enablePush) allImages;

  pushImagesScript =
    let
      pushScriptFor = image: ''
        ${pkgs.skopeo}/bin/skopeo copy $@ docker-archive:${image.image} docker://${image.registryPath}
      '';
    in
    pkgs.writeShellScript "push-images.sh" (concatMapStrings pushScriptFor enabledImages);
in
{
  options = {
    registry = {
      defaultPrefix = mkOption {
        description = mdDoc "Registry path prefix to use by default for all container images";
        type = types.str;
        default = "";
        example = "registry.example.com/foo-project";
      };

      pushImages = mkOption {
        description = mdDoc "Script to push all enabled images to container registries.";
        type = types.package;
        default = pushImagesScript;
      };

      willPushImages = mkOption {
        internal = true;
        type = types.bool;
        default = (builtins.length enabledImages) != 0;
      };
    };

    images = mkOption {
      description = mdDoc "Container images to build";
      type = types.attrsOf (types.submodule imageSubmodule);
      default = { };
    };
  };
}
