{ lib
, kubectl
, writeShellScript

, manifestFile
, globalLabels
, allowPruneOf
, resources
}:
with lib;
let
  labelToSelector = name: value: [ "--selector" "${name}=${value}" ];
  selectorArgs = escapeShellArgs (builtins.concatLists (mapAttrsToList labelToSelector globalLabels));

  getGvk = resource:
    let
      maybeSlash = if (builtins.match ".*/.*" resource.version) != null then "" else "/";
    in
    "${maybeSlash}${resource.version}/${resource.kind}";

  lookupResource = builtins.listToAttrs (builtins.map (v: { name = v.name; value = getGvk v; }) resources);

  pruneAllowArgs = escapeShellArgs (builtins.concatMap (v: [ "--prune-allowlist" (builtins.getAttr v.name lookupResource) ]) (builtins.filter (v: v.value) (attrsToList allowPruneOf)));
in
writeShellScript "apply.sh" ''
  ${kubectl}/bin/kubectl apply -f ${manifestFile} --prune ${pruneAllowArgs} ${selectorArgs} $@
''
