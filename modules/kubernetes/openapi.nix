{ config, lib, n2k, ... }:
with lib;
with n2k;
let
  cfg = config.kubernetes.openapi;

  # ---- Options generation ----

  definitions = cfg.source.spec.definitions;

  handleType = visited: namespace: {
    object = opts:
      if opts ? additionalProperties && !(opts ? properties)
      then
        let
          subOpt = openapiToOption opts.additionalProperties visited namespace;
        in
        {
          type = unsetOr (types.attrsOf subOpt.type);
          default = unset;
          defaultText = literalExpression "unset";
        }
      else
        let
          gvk =
            if opts ? x-kubernetes-group-version-kind
            then builtins.elemAt opts.x-kubernetes-group-version-kind 0
            else null;

          disallowedFields =
            if gvk != null
            then [ "status" ]
            else [ ];

          optionsOverride =
            if gvk != null && opts ? properties
            then {
              kind.default = gvk.kind;
              apiVersion.default = if gvk.group != "" then "${gvk.group}/${gvk.version}" else gvk.version;
            }
            else { };

          properties = if opts ? properties then builtins.removeAttrs opts.properties disallowedFields else { };
          options = recursiveUpdate (builtins.mapAttrs (_: subOpts: openapiToOption subOpts visited namespace) properties) optionsOverride;

          submodule = types.submodule ({ name, ... }: {
            inherit options;

            config =
              if gvk != null && opts ? properties
              then {
                metadata.name = mkDefault name;
                metadata.namespace = mkDefault namespace;
              }
              else { };
          });
        in
        {
          type = if gvk != null then submodule else unsetOr submodule;
        } // optionalAttrs (gvk == null) {
          default = unset;
          defaultText = literalExpression "unset";
        };
    string = opts:
      if opts ? format && opts.format == "int-or-string"
      then { type = unsetOr (types.either types.str types.int); default = unset; defaultText = literalExpression "unset"; }
      else { type = unsetOr types.str; default = unset; defaultText = literalExpression "unset"; }
    ;
    array = opts:
      let
        itemOption = openapiToOption opts.items visited namespace;
      in
      {
        type = unsetOr (types.listOf itemOption.type);
        default = unset;
        defaultText = literalExpression "unset";
      };
    integer = _: { type = unsetOr types.int; default = unset; defaultText = literalExpression "unset"; };
    number = _: { type = unsetOr types.float; default = unset; defaultText = literalExpression "unset"; };
    boolean = _: { type = unsetOr types.bool; default = unset; defaultText = literalExpression "unset"; };
  };

  openapiToOption = opts: visited: namespace:
    if opts ? "$ref"
    then
      let
        definitionName = builtins.replaceStrings [ "#/definitions/" ] [ "" ] opts."$ref";
      in
      lookupDefinitionType definitionName visited namespace
    else
      mkOption ((optionalAttrs (opts ? description) {
        description = mdDoc opts.description;
      }) // (
        if opts ? type
        then (builtins.getAttr opts.type (handleType visited namespace)) opts
        else {
          type = types.unspecified;
          default = unset;
          defaultText = literalExpression "unset";
        }
      ));

  lookupDefinitionType = name: visited: namespace:
    if builtins.any (v: v == name) visited
    then builtins.trace { inherit visited name; } (builtins.abort ("Recursion detected: " + name))
    else
      if builtins.hasAttr name cfg.specOverrides
      then builtins.getAttr name cfg.specOverrides
      else openapiToOption (builtins.getAttr name definitions) (visited ++ [ name ]) namespace;

  extractGVKs = name: definition:
    if definition ? x-kubernetes-group-version-kind
    then
      let
        makeType = lookupDefinitionType name [ ];
      in
      builtins.map
        (gvk:
          let
            gv = (if gvk ? group && gvk.group != "" then gvk.group + "/" else "") + gvk.version;
          in
          {
            "${gv}/${gvk.kind}" = makeType;
          }
        )
        definition.x-kubernetes-group-version-kind
    else [ ];

  gvks =
    attrsets.mergeAttrsList (builtins.concatLists (builtins.attrValues (builtins.mapAttrs extractGVKs definitions)));

  apiResources =
    let
      splitted = builtins.split "([^ \n]+) +(([^ \n]+) +)?([^ \n]+) +([^ \n]+) +([^ \n]+)\n" cfg.source.apiResources;
      matches = builtins.filter builtins.isList splitted;
      matchesToSet = list: rec {
        name = builtins.elemAt list 0;
        version = builtins.elemAt list 3;
        namespaced = (builtins.elemAt list 4) == "true";
        kind = builtins.elemAt list 5;
        makeOption = builtins.getAttr "${version}/${kind}" gvks;
      };
    in
    builtins.map matchesToSet matches;

  namespacedResources = builtins.filter (resource: resource.namespaced) apiResources;
  globalResources = builtins.filter (resource: !resource.namespaced) apiResources;

  resourcesToOptions = resources: namespace:
    let
      resourceToOption = resource: {
        name = resource.name;
        value = mkOption {
          description = mdDoc "Define resources of kind ${resource.kind} (${resource.version})";
          type = types.attrsOf (resource.makeOption namespace).type;
          default = { };
        };
      };
      kvs = builtins.map resourceToOption resources;
    in
    builtins.listToAttrs kvs;

  namespaceSubmodule = { name, ... }: {
    options = resourcesToOptions namespacedResources name;
  };
in
{
  options.kubernetes.openapi = {
    source = {
      spec = mkOption {
        description = mdDoc "JSON OpenAPI v2 spec of kubernetes API to target.";
        type = types.unspecified;
        default = builtins.fromJSON (builtins.readFile ./spec.json);
        defaultText = "<hidden>";
      };

      apiResources = mkOption {
        description = mdDoc "Kubernetes API resources table as output by `kubectl api-resources --verbs \"create\" --no-headers`.";
        type = types.str;
        default = builtins.readFile ./api-resources.txt;
        defaultText = "<hidden>";
      };
    };

    specOverrides = mkOption {
      description = mdDoc "Overrides for spec definition types";
      type = types.attrsOf types.unspecified;
      defaultText = "<hidden>";
      default = {
        "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaProps" = mkOption {
          description = mdDoc "JSONSchemaProps is a JSON-Schema following Specification Draft 4 (http://json-schema.org/).";
          type = types.unspecified;
        };
        "io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta" = mkOption {
          description = mdDoc "Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata";
          type = types.submodule {
            options = {
              annotations = mkOption {
                description = mdDoc "Annotations is an unstructured key value map stored with a resource that may be set by external tools to store and retrieve arbitrary metadata. They are not queryable and should be preserved when modifying objects. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations";
                type = unsetOr (types.attrsOf (types.str));
                default = unset;
                defaultText = literalExpression "unset";
              };

              labels = mkOption {
                description = mdDoc "Map of string keys and values that can be used to organize and categorize (scope and select) objects. May match selectors of replication controllers and services. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels";
                type = unsetOr (types.attrsOf (types.str));
                default = unset;
                defaultText = literalExpression "unset";
              };

              name = mkOption {
                description = mdDoc "Name must be unique within a namespace. Is required when creating resources, although some resources may allow a client to request the generation of an appropriate name automatically. Name is primarily intended for creation idempotence and configuration definition. Cannot be updated. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names#names";
                type = unsetOr types.str;
                default = unset;
                defaultText = literalExpression "unset";
              };

              namespace = mkOption {
                description = mdDoc "Namespace defines the space within which each name must be unique. An empty namespace is equivalent to the \"default\" namespace, but \"default\" is the canonical representation. Not all objects are required to be scoped to a namespace - the value of this field for those objects will be empty.\n\nMust be a DNS_LABEL. Cannot be updated. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces";
                type = unsetOr types.str;
                default = unset;
                defaultText = literalExpression "unset";
              };
            };
          };
        };
      };
    };

    resources = mkOption {
      description = mdDoc "All included resources";
      type = types.listOf (types.submodule {
        options = {
          name = mkOption { type = types.str; };
          version = mkOption { type = types.str; };
          namespaced = mkOption { type = types.bool; };
          kind = mkOption { type = types.str; };
        };
      });
      default = builtins.map (s: builtins.removeAttrs s [ "makeOption" ]) apiResources;
    };

    # debug = mkOption {
    #   type = types.unspecified;
    #   default = apiResources;
    # };
  };

  options.kubernetes.namespaces = mkOption {
    description = mdDoc "Namespaces to define resources within.";
    type = types.attrsOf (types.submodule namespaceSubmodule);
    default = { };
  };

  options.kubernetes.global = resourcesToOptions globalResources unset;
}
