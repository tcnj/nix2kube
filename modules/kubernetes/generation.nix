{ config, lib, n2k, ... }:
with lib;
let
  cfg = config.kubernetes;

  namespacedObjects = builtins.concatMap builtins.attrValues (builtins.concatMap builtins.attrValues (builtins.attrValues cfg.namespaces));
  globalObjects = builtins.concatMap builtins.attrValues (builtins.attrValues cfg.global);

  allObjects = globalObjects ++ namespacedObjects;
in
{
  config.kubernetes.output.objects = allObjects;
}
