{ ... }:
{
  imports = [
    ./generation.nix
    ./openapi.nix
    ./output.nix
  ];
}
