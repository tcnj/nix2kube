{ pkgs, config, lib, n2k, ... }:
with lib;
let
  cfg = config.kubernetes.output;

  hasGlobalLabels = (builtins.length (builtins.attrNames cfg.globalLabels)) != 0;

  labelOverrides =
    if hasGlobalLabels
    then {
      metadata.labels = cfg.globalLabels;
    }
    else { };

  objectToJson = object:
    let
      filteredObject = n2k.filterObject object;
      mergedWithOverrides = recursiveUpdate filteredObject labelOverrides;
    in
    "---\n" + (builtins.toJSON mergedWithOverrides) + "\n";

  objectsJSON = builtins.map objectToJson cfg.objects;

  mkDataOption = description:
    mkOption {
      description = mdDoc description;
      type = n2k.unsetOr types.unspecified;
      default = n2k.unset;
    };

  # objectType = types.submodule {
  #   options = {
  #     apiVersion = mkOption {
  #       description = mdDoc "APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources";
  #       type = types.str;
  #     };
  #     kind = mkOption {
  #       description = mdDoc "Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds";
  #       type = types.str;
  #     };

  #     metadata = {
  #       annotations = mkOption {
  #         description = mdDoc "Annotations is an unstructured key value map stored with a resource that may be set by external tools to store and retrieve arbitrary metadata. They are not queryable and should be preserved when modifying objects. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations";
  #         type = n2k.unsetOr (types.attrsOf (types.str));
  #         default = n2k.unset;
  #       };

  #       labels = mkOption {
  #         description = mdDoc "Map of string keys and values that can be used to organize and categorize (scope and select) objects. May match selectors of replication controllers and services. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels";
  #         type = n2k.unsetOr (types.attrsOf (types.str));
  #         default = n2k.unset;
  #       };

  #       name = mkOption {
  #         description = mdDoc "Name must be unique within a namespace. Is required when creating resources, although some resources may allow a client to request the generation of an appropriate name automatically. Name is primarily intended for creation idempotence and configuration definition. Cannot be updated. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names#names";
  #         type = types.str;
  #       };

  #       namespace = mkOption {
  #         description = mdDoc "Namespace defines the space within which each name must be unique. An empty namespace is equivalent to the \"default\" namespace, but \"default\" is the canonical representation. Not all objects are required to be scoped to a namespace - the value of this field for those objects will be empty.\n\nMust be a DNS_LABEL. Cannot be updated. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces";
  #         type = n2k.unsetOr types.str;
  #         default = n2k.unset;
  #       };
  #     };

  #     spec = mkDataOption "Spec defines the behavior of the object. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status";
  #     data = mkDataOption "Data contains the configuration data. Each key must consist of alphanumeric characters, '-', '_' or '.'. Values with non-UTF-8 byte sequences must use the BinaryData field. The keys stored in Data must not overlap with the keys in the BinaryData field, this is enforced during validation process.";
  #     binaryData = mkDataOption "BinaryData contains the binary data. Each key must consist of alphanumeric characters, '-', '_' or '.'. BinaryData can contain byte sequences that are not in the UTF-8 range. The keys stored in BinaryData must not overlap with the ones in the Data field, this is enforced during validation process. Using this field will require 1.10+ apiserver and kubelet.";
  #     immutable = mkOption {
  #       description = mdDoc "Immutable, if set to true, ensures that data stored in the ConfigMap cannot be updated (only object metadata can be modified). If not set to true, the field can be modified at any time. Defaulted to nil.";
  #       type = n2k.unsetOr types.boolean;
  #       default = n2k.unset;
  #     };
  #   };
  # };

  resourceToPruneOpt = resource: {
    name = resource.name;
    value = mkOption {
      description = mdDoc "Allow pruning of ${resource.version}/${resource.kind}.";
      type = types.bool;
      default = false;
    };
  };
in
{
  options.kubernetes.output = {
    objects = mkOption {
      description = mdDoc "All raw objects to be included in the generated manifest.";
      type = types.listOf (types.attrsOf types.unspecified);
      default = [ ];
    };

    globalLabels = mkOption {
      description = mdDoc "Labels to apply to all objects in the generated manifest.";
      type = types.attrsOf types.str;
      default = { };
    };

    manifest = mkOption {
      description = mdDoc "The final generated YAML manifest.";
      type = types.str;
      default = builtins.concatStringsSep "" objectsJSON;
    };

    manifestFile = mkOption {
      description = mdDoc "The final generated YAML manifest, as a file.";
      type = types.package;
      default = pkgs.writeText "manifest.yaml" cfg.manifest;
    };

    manifestPrettyFile = mkOption {
      description = mdDoc "The final generated YAML manifest, prettified, as a file.";
      type = types.package;
      default = pkgs.runCommand "manifest-pretty.yaml" { } ''
        cat ${cfg.manifestFile} | ${pkgs.yq}/bin/yq -y > $out
      '';
    };

    manifestPretty = mkOption {
      description = mdDoc "The final generated YAML manifest, prettified. Warning: this uses an IFD as a derivation is needed to perform the prettification. Prefer using manifestPrettyFile instead.";
      type = types.str;
      default = builtins.readFile cfg.manifestPrettyFile;
    };

    allowDangerousApplyScript = mkOption {
      # The author wishes to specifically disclaim any and all responsibility for any actions or consequences of setting this option to true.
      # Use at your own risk.
      type = types.bool;
      default = false;
      visible = false;
    };

    applyScript = mkOption {
      description = mdDoc "A script for applying the manifest using `kubectl --prune`. WARNING: will typically have unintended consequences if no `globalLabels` are set. See https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/#alternative-kubectl-apply-f-directory-prune";
      type = types.package;
      default =
        let
          warnOnEval =
            if hasGlobalLabels
            then v: v
            else if cfg.allowDangerousApplyScript
            then v: builtins.trace "WARNING: the generated applyScript uses `kubectl --prune` but no globalLabels are set. This might destroy unrelated resources when run!" v
            else builtins.abort "Refusing to generate an applyScript as no global labels have been set. Using `kubectl apply --prune` with no label selectors can destroy unrelated, potentially cluster-scoped, resources. There is no known case where this is a good idea. Consider setting `kubernetes.output.globalLabels` to something unique that identifies this deployment manifest."
          ;
        in
        warnOnEval (pkgs.callPackage ./output/apply-script.nix {
          inherit (cfg) manifestFile globalLabels allowPruneOf;
          inherit (config.kubernetes.openapi) resources;
        });
    };

    allowPruneOf = mkOption {
      description = mdDoc "Allow pruning the specified objects in the `applyScript`.";
      type = types.submodule {
        options = builtins.listToAttrs (builtins.map resourceToPruneOpt config.kubernetes.openapi.resources);
      };
      default = {
        configmaps = true;
        endpoints = true;
        persistentvolumeclaims = true;
        pods = true;
        replicationcontrollers = true;
        secrets = true;
        services = true;
        jobs = true;
        cronjobs = true;
        ingresses = true;
        daemonsets = true;
        deployments = true;
        replicasets = true;
        statefulsets = true;
        namespaces = true;
        persistentvolumes = true;
      };
    };
  };
}
