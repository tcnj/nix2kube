{ n2k, lib, pkgs, ... }:
let
  eval = n2k.evalModules [ ];
  # generate our docs
  optionsDoc = pkgs.nixosOptionsDoc {
    inherit (eval) options;
    warningsAreErrors = false;
  };

  docMarkdown = pkgs.runCommand "options-doc.md" { } ''
    cat ${./options.header.md} >> $out
    cat ${optionsDoc.optionsCommonMark} >> $out
  '';
in
pkgs.stdenv.mkDerivation {
  src = ./docs;
  name = "docs";

  buildInput = [ docMarkdown ];

  # mkdocs dependencies
  nativeBuildInputs = [
    pkgs.zola
  ];

  # symlink our generated docs into the correct folder before generating
  buildPhase = ''
    mkdir -p content
    ln -s ${docMarkdown} "./content/options.md"

    zola build
  '';

  # copy the resulting output to the derivation's $out directory
  installPhase = ''
    cp -r public $out
  '';
}
