{
  description = "Tools for generating kubernetes manifests using Nix";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }@inputs: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};

      lib = import ./lib { inherit pkgs; lib = nixpkgs.lib; };

      generateExample = module:
        let
          name = builtins.replaceStrings [ ".nix" ] [ "" ] (builtins.baseNameOf module);
          evaluated = lib.evalModules [ module ];

          copyPodmanScripts = pkgs.lib.mapAttrsToList
            (name': options: ''
              cp ${options.podmanScript} $out/${name}.run-${name'}.sh
            '')
            evaluated.config.images;

        in
        pkgs.runCommand ("example-" + name) { } (builtins.concatStringsSep "\n" (
          [
            ''
              mkdir $out

              echo Copying manifest for ${name}...
              cp ${evaluated.config.kubernetes.output.manifestPrettyFile} $out/${name}.yml
            ''
          ] ++ (pkgs.lib.optional evaluated.config.registry.willPushImages ''
            echo Copying push script for ${name}...
            cp ${evaluated.config.registry.pushImages} $out/${name}.push.sh
          '') ++ copyPodmanScripts
        ))
      ;

      examples = builtins.map generateExample (import ./examples);
    in
    {
      inherit lib;

      packages.docs = pkgs.callPackage ./options-doc.nix { n2k = lib; };
      packages.examples = pkgs.symlinkJoin {
        name = "examples";
        paths = examples;
      };
    }
  );

}
